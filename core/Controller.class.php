<?php
abstract class Controller {
//  sert de conteneur aux contrôleurs que le dev implémentera dans son appli.
    private $_get;
    private $_post;

    public function __construct($post,$get) {
        $post=$this->_post;
        $this->$post=$_POST;
        $get=$this->_get;
        $this->$get=$_GET;
    }
    
    protected function inputGet($get) {
        $get=$this->_get;
    }

    protected function inputPost($post) {
        $post=$this->_post;
    }

    public function render(/*chemin du fichier de vue,données à injecter dns la vue*/) {
        //  générer et retourner la vue demandée (si c'est le cas)
        /*
        $datas = array(
            "users"=> (new DAOUser())->getAll()
        );
        $this->render("liste_utilisateurs",$datas);
        *ou*
        $datas = array(
            "users"=> (new DAOUser())->getAll()
        );
        $this->render("listes/liste_utilisateurs",$datas);
        */
        /*
        le fichier contient une variable $users sur laquelle
        il va falloir boucler pour afficher les utilisateurs dans une table
        */
    }
    /*
    final protected function render($pathToView, $datas =null)
    */


}





