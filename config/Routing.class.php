<?php
class Routing {
    private $_config = array('./routing', '.json');  //    *** array serialize/deserialize ?
    private $_url = array('URI','en','tableau');   //  *** array idem
    private $_route = array('route', 'testée', 'en', 'tableau');    //  array *** idem + "clé de config" ?
    private $_controller = 'controleur de getValue()';
    private $_args = array('arguments', 'à passer au contrôleur', '= éléments variables', 'de l\'URI'); //  ***  array idem
    private $_methode = 'verbe http utilisé lors de la requête';

    public function __construct($config) {
        $config=$this->_config;
    }

    public function test() {
        return $this->_config;
    }

    public function execute() {
        //  déclenche le mécanisme de routage
        if($this->isEqual()) {
            $this->compare();
        }
    }

    private function isEqual() {
        //  booléen = résultat de la comparaison des hauteurs des 2 tableaux ***
        return  $this->_url === $this->_route;
    }

    private function getValue() {
        //  retourne le contrôleur correspondant à la route sélectionnée

    }

    private function addArgument() {
        //  ajoute l'élément variable de l'URI si l'élément en cours est censé être variable
    }

    private function compare() {
        //  conpare les éléments des 2 tableaux (url et route) : si les 2 correspondent, c'est que la route a été trouvée
        $url=$this->_url;
        $route=$this->_route;
        for($x=0;$x<count($this->_url);$x++) {
            if($url[$x] !== $route[$x]) {
                if($route[$x] == '(:)') {
                    return false;
                } else {
                    $this->addArgument($x);
                    $this->invoke($this->_controller, $this->_args);
                }
            }
        }
    }

    private function invoke($_controller, $_args) {
        //  invoquée quand le contrôleur a été sélectionné
        //  => crée un objet contrôleur 
        //  et invoque la méthode en y passant les arguments adéquats ***
        $obj = new self($_controller,$_args);
//        $obj->method();   *** méthode du contrôleur adéquate, via $_controller et $_args
//  Le mécanisme d'indirection (accès à l'adresse d'un objet) va permettre d'instancier un contrôleur grâce au texte du fichier de donfiguration   ***
    }

}


